﻿var user;
var autoScroll = true;
var app = angular.module("myapp", [ "firebase", "ngCookies" ]).config(function($routeProvider) {
	$routeProvider.when('/', {
		controller : LoginController,
		templateUrl : 'login.html'
	}).when('/chat', {
		controller : MyController,
		templateUrl : 'chat.html'
	}).otherwise({
		redirectTo : '/'
	});
});
function MyController($scope, $location, $cookies, angularFire) {
	$scope.name = $cookies.user;
	var prefix;
	$scope.messages = [];
	if (chrome.tabs) {
		chrome.tabs.getSelected(function(tab) {
			if (tab.url.indexOf("?") > 0) {
				prefix = MD5(tab.url.substring(0, tab.url.indexOf("?")));
			} else if (tab.url.indexOf("#") > 0) {
				prefix = MD5(tab.url.substring(0, tab.url.indexOf("#")));
			} else {
				prefix = MD5(tab.url);
			}
			var ref = new Firebase("https://" + prefix + ".firebaseio-demo.com/");
			angularFire(ref, $scope, "messages");
		});
	} else {
		if (window.location.href.indexOf("?") > 0) {
			prefix = MD5(window.location.href.substring(0, window.location.href.indexOf("?")));
		} else {
			prefix = MD5(window.location.href);
		}
		var ref = new Firebase("https://" + prefix + ".firebaseio-demo.com/");
		angularFire(ref, $scope, "messages");
	}

	$scope.addMessage = function(e) {
		if (e.keyCode != 13)
			return;
		if ($scope.messages.length >= 300) {
			$scope.messages.shift();
		}
		$scope.messages.push({
			from : $scope.name,
			time : new Date().format("MM月dd日 hh:mm"),
			body : $scope.msg
		});
		$scope.msg = "";
		document.getElementById("messagesDiv").scrollTop = document.getElementById("messagesDiv").scrollHeight;
	}

	$scope.changeUser = function() {
		$cookies.user = "";
		$location.path("/")
	}
	$scope.stopScroll = function() {
		autoScroll = !$scope.change;
	};
	refreshScroller();
	document.onmousedown = function() {
		autoScroll = false;
	}

	document.onmouseup = function() {
		autoScroll = !$scope.change;
	}
}

function LoginController($scope, $location, $cookies, angularFire) {
	$scope.userName = $cookies.user;
	if ($cookies.user) {
		$location.path("/chat")
	}
	$scope.addGlobal = function() {
		$cookies.user = $scope.userName;
	}
	$scope.persistUser = function() {
		if ($scope.persist == true) {
			$cookies.user = $scope.userName;
		}
	}
}

function refreshScroller() {
	if (autoScroll) {
		document.getElementById("messagesDiv").scrollTop = document.getElementById("messagesDiv").scrollHeight;
	}
	setTimeout(function() {
		refreshScroller();
	}, 300);
}

Date.prototype.format = function(mask) {
	var d = this;
	var zeroize = function(value, length) {
		if (!length)
			length = 2;
		value = String(value);
		for ( var i = 0, zeros = ''; i < (length - value.length); i++) {
			zeros += '0';
		}
		return zeros + value;
	};
	return mask
			.replace(
					/"[^"]*"|'[^']*'|\b(?:d{1,4}|m{1,4}|yy(?:yy)?|([hHMstT])\1?|[lLZ])\b/g,
					function($0) {
						switch ($0) {
						case 'd':
							return d.getDate();
						case 'dd':
							return zeroize(d.getDate());
						case 'ddd':
							return [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat' ][d.getDay()];
						case 'dddd':
							return [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ][d
									.getDay()];
						case 'M':
							return d.getMonth() + 1;
						case 'MM':
							return zeroize(d.getMonth() + 1);
						case 'MMM':
							return [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ][d
									.getMonth()];
						case 'MMMM':
							return [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
									'September', 'October', 'November', 'December' ][d.getMonth()];
						case 'yy':
							return String(d.getFullYear()).substr(2);
						case 'yyyy':
							return d.getFullYear();
						case 'h':
							return d.getHours() % 12 || 12;
						case 'hh':
							return zeroize(d.getHours() % 12 || 12);
						case 'H':
							return d.getHours();
						case 'HH':
							return zeroize(d.getHours());
						case 'm':
							return d.getMinutes();
						case 'mm':
							return zeroize(d.getMinutes());
						case 's':
							return d.getSeconds();
						case 'ss':
							return zeroize(d.getSeconds());
						case 'l':
							return zeroize(d.getMilliseconds(), 3);
						case 'L':
							var m = d.getMilliseconds();
							if (m > 99)
								m = Math.round(m / 10);
							return zeroize(m);
						case 'tt':
							return d.getHours() < 12 ? 'am' : 'pm';
						case 'TT':
							return d.getHours() < 12 ? 'AM' : 'PM';
						case 'Z':
							return d.toUTCString().match(/[A-Z]+$/);
							// Return quoted strings with the surrounding quotes
							// removed
						default:
							return $0.substr(1, $0.length - 2);
						}
					});
};
